unit MainView;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Rtti, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, PathDataFix,
  FMX.Objects, System.Diagnostics, FMX.Layouts, FMX.Memo, FMX.ListBox;

type
  TForm1 = class(TForm)
    pthPath: TPath;
    btnDraw: TButton;
    btnDrawHS: TButton;
    btnSyn: TButton;
    mmoPathData: TMemo;
    btnDrawPath: TButton;
    lblPathData: TLabel;
    lblDrawSrc: TLabel;
    cbbWrapMode: TComboBox;
    lbiOrigin: TListBoxItem;
    lbiFit: TListBoxItem;
    lbiStretch: TListBoxItem;
    lbiTile: TListBoxItem;
    procedure btnDrawClick(Sender: TObject);
    procedure btnDrawHSClick(Sender: TObject);
    procedure btnSynClick(Sender: TObject);
    procedure btnDrawPathClick(Sender: TObject);
    procedure OnChangeWrapMode(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.btnDrawClick(Sender: TObject);
begin
  pthPath.Data.Data :=
    'M23.677 , 32.741  ' +
    'c  6.469,1.508,11.327,7.069,11.327,13.679' +
    'v50.754' +
    'c0,1.56-1.355,2.825-2.966,2.825' +
    'H2.94' +
	  'C 1.33,100 0,98.734 0,97.175' +
    'V46.42' +
    'c 0-6.623, 4.807 -12.196, 11.327-13.679' +
    'V10.023' +
    'h12.35' +
    'V32.741' +
    'z' +
    'M64.024,10.79' +
    'c-0.895 -0.128 -1.585 -0.856 -1.585 -1.738' +
    'c0 -0.64,0.384 -1.202,0.946 -1.521' +
    'c-0.562 -0.294 -0.946 -0.87 -0.946 -1.521' +
	  'c0 -0.972,0.817 -1.739,1.841 -1.739' +
    'h9.205' +
    'c1.022,0,1.841,0.767,1.841,1.739' +
    'c0,0.678 -0.384,1.24 -0.972,1.521' +
	  'c0.562,0.307,0.972,0.856,0.972,1.521' +
    'c0,0.882 -0.665,1.61 -1.56,1.726l11.147,28.676' +
    'c0.69,1.866,1.048,3.938,1.048,5.996v50.805' +
	  'c0,1.918 -1.636,3.478 -3.656,3.478' +
    'H55.484' +
    'c -2.046,0 -3.656 -1.56 -3.656 -3.478' +
    'V45.449' +
    'c0 -2.058,0.333 -4.129,1.022 -5.996' +
    'L64.024,10.79' +
    'z';
end;

procedure TForm1.btnDrawHSClick(Sender: TObject);
begin
  pthPath.Data.Data := 'm20,20 L40,10 50,30 140,100 C20,30 100,180 250,200 A15,15,90,90,90,150,150 z';
end;

procedure TForm1.btnDrawPathClick(Sender: TObject);
begin
  pthPath.Data.Data := mmoPathData.Text;
end;

procedure TForm1.btnSynClick(Sender: TObject);
begin
  pthPath.Data.Data := 'M0,350 l 50,-25 a25,25 -30 0,1 50,-25 l 50,-25 a25,50 -30 0,1 50,-25 l 50,-25 a25,75 -30 0,1 50,-25 l 50,-25 a25,100 -30 0,1 50,-25 l 50,-25';
end;

procedure TForm1.OnChangeWrapMode(Sender: TObject);
begin
  if Sender is TComboBox then
    case TComboBox(Sender).Selected.Tag of
      0: pthPath.WrapMode := TPathWrapMode.pwOriginal;
      1: pthPath.WrapMode := TPathWrapMode.pwFit;
      2: pthPath.WrapMode := TPathWrapMode.pwStretch;
      3: pthPath.WrapMode := TPathWrapMode.pwTile;
    end;
end;

end.
