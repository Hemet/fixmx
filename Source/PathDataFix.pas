unit PathDataFix;

interface

uses
  System.SysUtils, System.Types, FMX.Types, FMX.Forms, System.Classes, System.Character;

type
  {*
    ����������:
      1. ������: ������ ����� ������������� ���������� �� �������� �������� �� ����������.
      2. ������: ������� � ���� ������� �� ������ ��� ���������� ��������� ����������.
      3. ������: ������� ���������� ������� ��������� ���������� (�������� ��������� ������������).
      4. ������: ��������, ������ ������ ������� (������ ��������� ����� ���������).
      5. ����������: ������ ��� ��������� ���������� ������� "c".
    ���������:
      1. ��������� ���� "c".
      2. ��������� ����� "T(t)".
      3. ��������� ����� "Q(q)".
      4. � ������ ������ ������� ������ ������ ���������� ESVGParseError � ESVGIllegalSymbol,
         � ������� ���������� ���������� � �������� ������.
      5. ������� RelToAbs ��� ��������� ������������� ��������� � ����������.
      6. ������� ReflectPoint ���������� ��������� ����� ������������ ������ �����,
         ��� ���� ��������� ��� ������� �������� ��������������� �������, ������������
         ����� ����� �������.
     �����������:
      1. TStringBuilder ������ �� ������������, �.�. ���� ����������� ����������
         ���� � �������� � ������.
      2. ����� ����� repeat until � SetPathString.
      // �������:
      3. �������� ������� ��������� � �������������� ������������ ��� ������� �������,
         ������ ������� ������������ ������ ����������, � ���� ������� ��������
         ������������� � �������� � ����������� ������������.
      4. ��-��������, ������ �� ������ ���� ������������ ������ ����� ����,
         ��� �������� � ����������� ��� ����������� ��� ����������� � ������.
  *}
  TPathSymbol = (psSign, psDigit, psDot, psExp, psComma, psWSP, psCommand);
  TPathSymbols = set of TPathSymbol;

  ESVGParseError = class(Exception);
    ESVGPathParseError = class(ESVGParseError)
    private
      fPosition: Integer;
      fExpected: TPathSymbols;
      fFound: Char;
    public
      constructor Create(aPosition: Integer; aExpected: TPathSymbols; aFound: Char);
      property Position: Integer read fPosition;
      property Expected: TPathSymbols read fExpected;
      property Found: Char read fFound;
    end;

    ESVGIllegalSymbol = class(ESVGParseError)
    private
      fSymbol: Char;
    public
      constructor Create(aSymbol: Char);
      property Symbol: Char read fSymbol;
    end;

  TPathDataFix = class helper for TPathData
  private
    procedure SetPathString(const Value: string);
    function GetPathString: string;
    procedure RelToAbs(var aP: TPointF);
    function ReflectPoint(const aP, aBaseP: TPointF): TPointF;
    function GetPointFromStr(const S: string; var Pos: Integer; IsRelative: Boolean = False): TPointF;
    // fix bug with negative float without WSP
    function GetNum(const S: string; var Pos: Integer): string;
    function GetCommand(const S: string; var Pos: Integer): Char;
    function SymbolOf(aChar: Char): TPathSymbol;
  public
    property Data: string read GetPathString write SetPathString stored False;
  end;

implementation

{ TPathDataFix }

function TPathDataFix.GetNum(const S: string; var Pos: Integer): string;

type
  TParserAction = (paAdd, paSkip);

const
  startState = [psSign, psDigit, psDot, psWSP, psComma];

var
  acceptableSymbols: TPathSymbols;
  acceptableOnlyOnce: TPathSymbols;
  buf: string;

  function BufferContainsValidNumber: Boolean;
  begin
    Result := (buf <> '') and (acceptableSymbols * [psWSP, psComma, psCommand] <> []);
  end;

  function EndOfNumber: Boolean;
  var
    separatorsAfterNumber: Boolean;
    nextNegativeNumber: Boolean;
    unknownSymbol: Boolean;
  begin
    if BufferContainsValidNumber then
    begin
      // ����� ����� ����������� ����������� � �� ��������
      separatorsAfterNumber := CharInSet(S.Chars[Pos], [' ', ',', #9, #10, #13]);
      nextNegativeNumber := (S.Chars[Pos] = '-') and not (psSign in acceptableSymbols);
      // ���� ������ �� ����� ���� ������ ����� � ������������ ����� ���,
      // ������� ����������� ������ �� ���������� ����� � �������. ����� ������� ����������� :)
      unknownSymbol := not '-+.Ee0123456789 '#9#10#13.Contains(S.Chars[Pos]);
      Result := separatorsAfterNumber or nextNegativeNumber or unknownSymbol;
    end
    else
      Result := False;
  end;

  procedure ProcessSymbol(const aChar: Char; doOnAccept: TParserAction;
    aMayProceedWith: TPathSymbols; aNotAcceptableAnymore: Boolean);
  var
    sm: TPathSymbol;
  begin
    sm := SymbolOf(aChar);
    if sm in acceptableSymbols then
    begin
      if doOnAccept = paAdd then
        buf := buf + s.Chars[Pos];
      Inc(Pos);
    end
    else
      raise ESVGPathParseError.Create(Pos, acceptableSymbols, aChar);
    if aNotAcceptableAnymore then
      acceptableOnlyOnce := acceptableOnlyOnce  + [sm];
    acceptableSymbols := aMayProceedWith - acceptableOnlyOnce;
  end;

begin
  acceptableSymbols := startState;
  acceptableOnlyOnce := [];
  buf := '';

//  [sign][digits][.][digits][E(e)][sign][digits]
  while (Pos < S.Length) and not EndOfNumber do
  begin
    case S.Chars[Pos] of
      '-', '+':
          ProcessSymbol(S.Chars[Pos], paAdd, [psDigit, psDot], False);
      '.':
          ProcessSymbol(S.Chars[Pos], paAdd, [psDigit, psExp, psComma, psWSP, psCommand], True);
      'E', 'e':
          ProcessSymbol(S.Chars[Pos], paAdd, [psDigit, psSign], True);
      '0'..'9':
          ProcessSymbol(S.Chars[Pos], paAdd, [psDigit, psDot, psExp, psComma, psWSP, psCommand], False);
      ',':
          ProcessSymbol(S.Chars[Pos], paSkip, startState, True);
      ' ', #10, #13, #9:
          ProcessSymbol(S.Chars[Pos], paSkip, startState, False);
    else
        raise ESVGPathParseError.Create(Pos, acceptableSymbols, S.Chars[Pos]);
    end;
  end;

  Result := buf;
end;

function TPathDataFix.GetPathString: string;
begin
  Result := Self.GetPathString;
end;

function TPathDataFix.GetPointFromStr(const S: string;
  var Pos: Integer; IsRelative: Boolean = False): TPointF;
var
  X, Y: string;
begin
  //TODO: ���� ����� �������� ���, ������� �� ��������, ��� ��� ������� ����� �������� � ������ �����������
  X := GetNum(S, Pos);
  Y := GetNum(S, Pos);
  Result := PointF(StrToFloat(X, USFormatSettings), StrToFloat(Y, USFormatSettings));
  if IsRelative then
    RelToAbs(Result);
end;

function TPathDataFix.GetCommand(const S: string; var Pos: Integer): Char;
begin
  Result := #0;
  while (Pos < S.Length) and CharInSet(S.Chars[Pos],[' ',  #10, #13, #9]) do
    Inc(Pos); // ���� �������� ������ ��� ������� �������� � ������ ����

  if 'zmlchvsqtaZMLCHVSQTA'.Contains(S.Chars[Pos]) then
  begin
    Result := S.Chars[Pos];
    Inc(Pos);
  end;
end;

function TPathDataFix.ReflectPoint(const aP, aBaseP: TPointF): TPointF;
begin
  Result.X := 2 * aBaseP.X - aP.X;
  Result.Y := 2 * aBaseP.Y - aP.Y;
end;

procedure TPathDataFix.RelToAbs(var aP: TPointF);
var
  lp: TPointF;
begin
  lp := LastPoint;
  ap.X := lp.X + aP.X;
  ap.Y := lp.Y + aP.Y;
end;

procedure TPathDataFix.SetPathString(const Value: string);
var
  cmd: Char;
  R, CP1, CP2, CP3, ctrlP: TPointF;
  Angle: Single;
  large, sweep: Boolean;
  Pos: Integer;
  PointFTmp: TPointF;
  ctrlPIsSet: Boolean;
  readAsRel: Boolean;
  coord: Single;

  procedure SkipToNextToken;
  var
    state: TPathSymbols;
  begin
    state := [psWSP, psComma];
    while Pos < Value.Length do
      case Value.Chars[Pos] of
        ' ', #9, #10, #13:
          Inc(Pos);
        ',':
          begin
            if psComma in state then
            begin
              state := state - [psComma];
              Inc(Pos);
            end
            else
              raise ESVGPathParseError.Create(Pos, state, Value.Chars[Pos]);
          end;
      else
        Break; // ����������� �����������
      end;
  end;

  function NextSequenceOrEOL: Boolean;
  begin
    SkipToNextToken;
    Result := 'zmlchvsqtaZMLCHVSQTA'.Contains(Value.Chars[Pos]) or (Pos >= Value.Length);
  end;

  function IsRelCommand(aCommand: Char): Boolean; inline;
  begin
    Result := IsLower(aCommand);
  end;

begin
  ctrlPIsSet := False;
  SetLength(Self.FPathData, 0);
  Pos := 0;
  // �������� ����� � ������� ��� ������ ������� ������������, �� �� �������
  // ������ ���������� �� ��������.
  try
    while Pos < Value.Length do
    begin
      cmd := GetCommand(Value, Pos);
      readAsRel := IsRelCommand(cmd);
      case cmd of
      'z', 'Z':
        ClosePath;

      'm', 'M':
        repeat
          CP1 := GetPointFromStr(Value, Pos, readAsRel);
          MoveTo(CP1);
        until NextSequenceOrEOL;

      'l', 'L':
        repeat
          CP1 := GetPointFromStr(Value, Pos, readAsRel);
          LineTo(CP1);
        until NextSequenceOrEOL;

      'c', 'C':
        repeat
          CP1 := GetPointFromStr(Value, Pos, readAsRel);
          CP2 := GetPointFromStr(Value, Pos, readAsRel);
          CP3 := GetPointFromStr(Value, Pos, readAsRel);
          CurveTo(CP1, CP2, CP3);
        until NextSequenceOrEOL;

      's', 'S':
        repeat
          CP1 := GetPointFromStr(Value, Pos, readAsRel);
          CP2 := GetPointFromStr(Value, Pos, readAsRel);
          SmoothCurveTo(CP1, CP2);
        until NextSequenceOrEOL;

      'h', 'H':
        begin
          coord := StrToFloat(GetNum(Value, Pos), USFormatSettings);
          if readAsRel then
            coord := coord + LastPoint.X;
          HLineTo(coord);
        end;

      'v', 'V':
        begin
          coord := StrToFloat(GetNum(Value, Pos), USFormatSettings);
          if readAsRel then
            coord := coord + LastPoint.Y;
          VLineTo(coord);
        end;

      'q', 'Q':
        repeat
          CP1 := GetPointFromStr(Value, Pos, readAsRel);
          CP2 := GetPointFromStr(Value, Pos, readAsRel);
          ctrlP := CP1; // save last control point
          Self.QuadCurveTo(CP1, CP2);
        until NextSequenceOrEOL;

      't', 'T':
        repeat
          CP2 := GetPointFromStr(Value, Pos, readAsRel);
          if ctrlPIsSet then
            CP1 := ReflectPoint(ctrlP, LastPoint)
          else
            CP1 := LastPoint;
          ctrlP := CP1;
          Self.QuadCurveTo(CP1, CP2);
        until NextSequenceOrEOL;

      'a', 'A':
        repeat
          CP1 := LastPoint;
          R := GetPointFromStr(Value, Pos);
          Angle := StrToFloat(GetNum(Value, Pos), USFormatSettings);
          PointFTmp := GetPointFromStr(Value, Pos);
          large := PointFTmp.X = 1;
          sweep := PointFTmp.Y = 1;
          CP2 := GetPointFromStr(Value, Pos, readAsRel);
          Self.AddArcSvg(CP1, R, Angle, large, sweep, CP2);
        until NextSequenceOrEOL;
      else
        raise ESVGPathParseError.Create(Pos, [psCommand], cmd);
      end;

      // �������� T(t) ���������� ��������� ����������� ����� ��������� ���������� ������.
      ctrlPIsSet := CharInSet(cmd, ['Q', 'q', 'T', 't']);
    end;
  except
    on ESVGParseError do
    begin
      // ������������ ������ 1.1 �������, ����� ���������� �� ������ �� ����������
      // ����������� ��������� � ��������� ���������� ����������.
      // ������� ����������, ��� ������ � ������ ����������.
    end
  end;
  Self.FRecalcBounds := True;
  if Assigned(Self.FOnChanged) then
    Self.FOnChanged(Self);
end;


function TPathDataFix.SymbolOf(aChar: Char): TPathSymbol;
begin
  case aChar of
    '-', '+': Result := psSign;
    '.': Result := psDot;
    'E', 'e': Result := psExp;
    '0'..'9': Result := psDigit;
    ',': Result := psComma;
    ' ': Result := psWSP;
    'M', 'm', 'Z', 'z', 'L', 'l', 'H', 'h',
    'V', 'v', 'C', 'c', 'S', 's', 'Q', 'q',
    'T', 't', 'A', 'a': Result := psCommand;
  else
    raise ESVGIllegalSymbol.Create(aChar);
  end;
end;

{ ESVGPathParseError }

constructor ESVGPathParseError.Create(aPosition: Integer;
  aExpected: TPathSymbols; aFound: Char);
var
  expectStr: string;
begin
  fPosition := aPosition;
  fExpected := aExpected;
  fFound := aFound;
  if psSign in aExpected then
    expectStr := expectStr + QuotedStr('-') + ' or ';
  if psDigit in aExpected then
    expectStr := expectStr + '0..9 or ';
  if psDot in aExpected then
    expectStr := expectStr + QuotedStr('.') + ' or ';
  if psExp in aExpected then
    expectStr := expectStr + QuotedStr('E') + ' or ';
  if psComma in aExpected then
    expectStr := expectStr + QuotedStr(',') + ' or ';
  if psWSP in aExpected then
    expectStr := expectStr + 'whitespace or ';
  if psCommand in aExpected then
    expectStr := expectStr + 'draw-to command or ';
  expectStr := expectStr.Substring(0, expectStr.Length - 4);
  Message := 'Error occured at position ' + IntToStr(fPosition) + ': expect ' + expectStr + ' but found ' + QuotedStr(fFound);
end;

{ ESVGIllegalSymbol }

constructor ESVGIllegalSymbol.Create(aSymbol: Char);
begin
  fSymbol := aSymbol;
  Message := 'Illegal symbol found: ' + aSymbol;
end;

end.
